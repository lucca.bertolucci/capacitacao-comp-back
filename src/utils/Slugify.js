import slugify from "slugify";

export default str => {
    return slugify(str, {
        lower: true,
        replacement: '-',
        remove: /[\X21-\X2F\X3A-\X40\X5B-\X50\X7B-\X7E]/g
    })
  
}