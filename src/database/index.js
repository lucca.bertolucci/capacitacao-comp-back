import mongoose from 'mongoose'; //fazer conexão entre node e mongodb

mongoose.connect('mongodb://127.0.0.1/site', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false
});
mongoose.Promise = global.Promise; // fuçoes assincronas

export default mongoose;