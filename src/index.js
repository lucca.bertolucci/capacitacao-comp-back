import express, { Router } from "express";
import bodyParser from "body-parser";
import {Site} from '@/app/controllers'
 
const app = express();
const port = 3000;

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.json())
app.use('/site', Site);

console.log(`Servidor rodando no link http://localhost:${port}`);
app.listen(port);

