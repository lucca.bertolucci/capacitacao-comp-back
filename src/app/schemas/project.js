import mongoose from '@/database';
import slugify from "@/utils/slugify";

const ProjectSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true,
        unique: true
    },
    slug: {
        type: String,
        unique: true,
    },
    description: {
        type: String,
        required: true,
    },
    category: {
        type: String,
        required: true,
    },
    createdAt: {
        type: Date,
        default: Date.now
    }
    

});

ProjectSchema.pre('save', function(next){
    const title = this.title;
    this.slug = slugify(title)
})

export default mongoose.model('project', ProjectSchema)