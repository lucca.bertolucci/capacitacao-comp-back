import { Router } from 'express';
import project from '@/app/schemas/project';
import slugify from '@/utils/slugify';

const router = new Router()

router.get('/', (req, res) => {
    project.find() 
    .then(data => {
        const project = data.map(project => {
             return {title: project.title, category: project.category};
        });
        res.send(project);
    })
    .catch(error => {
        console.error('Erro ao obter projeto no banco de dados', error);
        res
        .status(400)
        .send({
            Error:
            'Não foi possível obter os dados do projeto. tente novamente',
        });
    
    });
});

router.get('/:projectSlug', (req, res) => {
    project.findOnde({slug: req.params.projecSlug})
    .then(project => {
        res.send(project);
    })
    .catch(error => {
        console.error('Erro ao obter projeto no banco de dados', error);
        res
        .status(400)
        .send({
            Error:
            'Não foi possível obter os dados do projeto. tente novamente',
        });
    
    });

});
router.post('/', (req, res) => {
    const { title, slug, description, category } = req.body;
    project.create({ title, slug, description, category })
    .then(projectsss => {
        console.log(req.body) 
        return res.status(200).send(projectsss);
    })
    .catch(error => {
        console.error('Erro ao salvar novo projeto no banco de dados', error);
        return res.status(400).send({
            Error:
             'Não foi possível salvar seu projeto. Verifique os dados e tente novamente',
        });
    });
});

router.put('/:projectId', (req, res) => {
    const {title, description, category} = req.body;
    let slug = undefined;
    if(title) {
        slug =  slugify(title);

    }
    project.findByIdAndUpdate(req.params.projectid,
    {title, slug, description, category},
    {new: true}
     )
    .them(project => {
        res.status(200).send(project);
    })
    .catch(error => {
        console.error('Erro ao salvar projeto no banco de dados', error);
        res
        .status(400)
        .send({
            Error:
             'Não foi possível salvar seu projeto. Verifique os dados e tente novamente',
        });
    });
});

router.delete('/:projectId', (req, res) => {
    project.findByIdAndRemove(req.params.projectid)
    .then( ()=> {
        res.send({message: 'projeto removido com sucesso' });
    })
    .catch(error => {
        console.error('Erro ao remover projeto do bando de dados', error);
        res
        .status(400)
        .send({ message: 'erro ao remover projeto, tente novamente' });
    })
});

export default router;




