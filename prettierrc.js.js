module.exports = {
        bracketSpacing: true,
        jsxBracketSamelLine: true,
        singleQuote: true,
        trailingComma: "all",
};